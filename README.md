# task-management

This project is an extended version of TODO application, where in a task can have multiple sub tasks. There can be owners assigned to the task, and statistics based upon the estimated and actual effort required to complete a task.